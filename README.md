# Art Institute of Chicago Artwork Browser

Since Eventric is at the intersection of art and technology, I thought it would be fun to have my SPA focus on art! This app allows you to browse artwork in The Art Institute of Chicago's public records.

## Application Overview

![Overview](docImages/overview.png)

You can click on the cards to view more details about the artwork

<img src='docImages/card.png' width='300px' />

You can search through the artwork via the search box.

<img src='docImages/search.png' height='50px' />

Scroll to the bottom of the page to browse through pages of artwork

<img src='docImages/pagination.png' height='50px' />

## To run locally

In the project directory, you can run:

### `npm i`

To install dependencies. Then run

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Implementation Details

This app was written using React and Typescript.
[Material-UI](https://mui.com/material-ui/getting-started/overview/) component library was used to give the page a cohesive design and to setup theming.

The Art Institute of Chicago has a public api that returns details about artwork that has been displayed at the museum.
[View the API](https://api.artic.edu/docs/#introduction)

Endpoints used:

- `/artworks/search`: retrieves the artwork ids that should be displayed on the provided page (passed via query params)
- `/artworks/{id}`: retrieves metadata about the artwork with the provided id, including the `image_id` which is used in the img src for the artwork
