import React from 'react';
import './App.scss';
import { MainPage } from './components/MainPage';

const App = ()  => {
  return (
    <MainPage />
  );
}

export default App;
