
export interface Artwork {
	id: number;
	title: string;
	imageURL: string;
	altTitles?: string[];
	artistDetails: string;
	placeOfOrigin: string;
	artworkType: string;
}