import { experimental_extendTheme as extendTheme } from "@mui/material";

export const getTheme = () => {
  return extendTheme({
		typography: {
			fontFamily: 'Nanum Myeongjo, serif',
			h1: {
				fontSize: 48
			},
			h2: {
				fontSize: 26
			},
			h3: {
				fontSize: 24
			},
			h4: {
				fontSize: 20
			},
			h5: {
				fontSize: 18
			},
			h6: {
				fontSize: 16,
				fontWeight: 'bold'
			},
			body1: {
				fontSize: 12,
				fontWeight: 'bolder'
			},
			body2: {
				fontSize: 12,
				fontWeight: 'lighter'
			}
		},
		components: {
			MuiCard: {
				styleOverrides: {
					root: ({ theme }) => ({
						backgroundColor: theme.vars.palette.secondary.main,
						'&:hover': {
							border: '1px solid #B60235'
						}
					}),
				},
			},
			MuiSwitch: {
				styleOverrides: {
					thumb: {
						backgroundColor: '#B60235'
					},
				},
			},
			MuiButton: {
				styleOverrides: {
					root: ({ theme }) => ({
						color: theme.vars.palette.info.main,
						backgroundColor: theme.vars.palette.secondary.main,
						'&:hover': {
							border: '1px solid #B60235',
							backgroundColor: theme.vars.palette.secondary.main,
						}
					}),
				}
			}
		},
		colorSchemes: {
			light: {
				palette: {
					primary: {
						main: '#fff' 
					},
					secondary: {
						main:  '#faf0e6' 
					},
					info: {
						main: '#000000de'
					}
				}
			},
			dark: {
				palette: {
					primary: {
						main: '#000',
					},
					secondary: {
						main: '#818589'
					},
					info: {
						main: '#fff'
					}
				}
			}
		}
	});
}