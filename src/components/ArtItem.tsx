import React, { useState, useEffect, useCallback } from 'react';
import { Artwork } from '../types';
import { Card, Typography } from '@mui/material';
import { getArtwork } from '../api/getArtwork';
import { useSpring, a } from "@react-spring/web";
import { SkeletonLoader } from './Loading/SkeletonLoader';
import styles from '../styles/ArtItem.module.scss';
import { ErrorDisplay } from './Error/ErrorDisplay';

interface Props {
  id: string;
}

export const ArtItem: React.FC<Props> = ({ id }) => {
  const [artwork, setArtwork] = useState<Artwork | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [flipped, set] = useState(false);

  const fetchArtwork = useCallback(() => {
    getArtwork({ id }).then((resp) => resp.data ? setArtwork(resp.data) : setError(resp.error))
  }, [id]);

  useEffect(() => {
    fetchArtwork();
  }, [fetchArtwork]);

  const { transform, opacity } = useSpring({
    opacity: flipped ? 1 : 0,
    transform: `perspective(600px) rotateY(${flipped ? 180 : 0}deg)`,
    config: { mass: 5, tension: 500, friction: 80 }
  });

  if (error) {
    return <ErrorDisplay message={error} retry={fetchArtwork}/>
  }

  if(!artwork) {
    return <SkeletonLoader />;
  }

  return (
    <div onClick={() => set((state) => !state)} style={{ height: '100%'}}>
      {!flipped && <a.div style={{ opacity: opacity.to((o) => 1 - o), transform, height: '100%' }}>
          <Card className={styles.artItemFront}>
            <img src={artwork.imageURL} style={{ maxWidth: 200, marginBottom: '8px' }} alt={artwork.title}/>
            <Typography variant='caption'>{artwork.title}</Typography>
          </Card>
        </a.div>}
        {flipped && <a.div style={{
          opacity,
          transform,
          rotateY: '180deg',
          height: '100%'
        }}>
          <Card className={styles.artItemBack}>
            <Typography variant='h4' className={styles.title}>{artwork.title}</Typography>
            {artwork.altTitles?.length && 
            <div>
              <Typography variant='h6'>Alternative Names: </Typography>
              <ul className={styles.altTitlesList}>
                {artwork.altTitles.map((title) => <li><Typography variant='body2'>{title}</Typography></li>)}
              </ul>
            </div>}
            <div className={styles.details}>
              <span className={styles.artDetail}>
                <Typography variant='body1'>Artist: </Typography>
                <Typography variant='body2'>{artwork.artistDetails}</Typography>
              </span>
              <span className={styles.artDetail}>
                <Typography variant='body1'>Place of origin: </Typography>
                <Typography variant='body2'>{artwork.placeOfOrigin}</Typography>
              </span>
              <span className={styles.artDetail}>
                <Typography variant='body1'>Art type: </Typography>
                <Typography variant='body2'>{artwork.artworkType}</Typography>
              </span>
            </div>
          </Card>
        </a.div>}
    </div>
  )
}



