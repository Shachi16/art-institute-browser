import React, { useState } from 'react';
import { ArtList } from './ArtList';
import { Typography, Pagination, Paper, Experimental_CssVarsProvider as CssVarsProvider, Switch, FormControlLabel } from '@mui/material';
import { getTheme } from '../utils/theme';
import styles from '../styles/MainPage.module.scss';
import { SearchBar } from './SearchBar';
import logo from '../resources/art-institute-logo.png';


export const MainPage: React.FC = () => {
	const [searchQuery, setSearchQuery] = useState<string>('');
	const [page, setPage] = useState(1);
	const [pageCount, setPageCount] = useState(0);
  const [darkMode, setDarkMode] = useState(false);

	const theme = getTheme();

	const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

	const toggleDarkMode = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDarkMode(event.target.checked);
  };

	return (
		<CssVarsProvider theme={theme}>
			<div className={styles.main} data-mui-color-scheme={darkMode ? 'dark' : 'light'}>
				<Paper className={styles.header} variant='outlined' square>
					<div className={styles.headerTitle}>
						<img src={logo} width='100px' alt='Art Institute of Chicago logo'/>
						<div className={styles.headerText}>
							<Typography variant='h1'>The Art Institute of Chicago</Typography>
							<Typography variant='h4'>Browse the artwork on display at The Art Institute of Chicago</Typography>
						</div>
					</div>
					<div className={styles.inputs}>
						<SearchBar onChange={setSearchQuery}/>
						<FormControlLabel
							value='end'
							control={<Switch
								checked={darkMode}
								onChange={toggleDarkMode}
								color='error'
							/>}
							label='Dark Mode'
							labelPlacement='top'
       			/>
					</div>
				</Paper>
				<ArtList searchQuery={searchQuery} page={page} setPageCount={setPageCount}/>
				{pageCount > 0 && <Pagination className={styles.pagination} count={pageCount} onChange={handlePageChange} showFirstButton showLastButton />}
			</div>
		</CssVarsProvider>
	)
}