import React, { useState, useEffect, useCallback } from 'react';
import { Grid } from '@mui/material';

import { searchArtwork } from '../api/searchArtwork';
import { ArtItem } from './ArtItem';
import { SkeletonGrid } from './Loading/SkeletonGrid';
import { ErrorDisplay } from './Error/ErrorDisplay';

interface Props {
	searchQuery: string;
	page: number;
	setPageCount: (count: number) => void
}

export const ArtList: React.FC<Props> = ({searchQuery, page, setPageCount}) => {
	const [artworkIds, setArtworkIds] = useState<string[]>([]);
	const [error, setError] =  useState<string | null>(null)

	const fetchArtWork = useCallback(() => {
		searchArtwork({searchQuery, page}).then((resp) => {
			console.log(resp)
			if(resp.data) {
				setArtworkIds(resp.data.ids)
				//The art institutes API returns a 403 error due to too many requests after page 83 so we cap it here
				setPageCount(resp.totalPages < 84 ? resp.totalPages : 83)
			}
			else {
				setError(resp.error)
			}
		})
	}, [searchQuery, page, setPageCount])

	useEffect(() => {
		fetchArtWork();
	}, [fetchArtWork])

	if (error) {
		return <ErrorDisplay message={error} retry={fetchArtWork}/>
	}

	if (!artworkIds.length) {
		return <SkeletonGrid />	
	}

  return (
		<Grid container alignItems='stretch' spacing={2} padding={4}>
			{artworkIds.map((id) => 
				<Grid item xs={12} sm={6} md={4} lg={3} marginBottom={2}>
					<ArtItem key={id} id={id}/>
				</Grid>
			)}
		</Grid>
	)
}

