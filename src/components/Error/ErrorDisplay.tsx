import React from 'react';
import { Button, Typography } from '@mui/material';
import styles from '../../styles/Error.module.scss';

interface Props {
  message: string
  retry: () => void;
}

export const ErrorDisplay:React.FC<Props> = ({ message, retry }) => {
  return (
    <div className={styles.errorPage}>
      <Typography variant='h3'>{message}</Typography>
      <Button variant='outlined' color='secondary' onClick={retry}>Retry</Button>
    </div>
  )
}