import React from 'react';
import { TextField } from '@mui/material';

interface Props {
	onChange: (input: string) => void;
}

export const SearchBar: React.FC<Props> = ({ onChange }) => {
	return (
		<TextField
			id='search-bar'
			variant='standard'
			placeholder='Search the artwork'
			size='medium'
			type='search'
			onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
        onChange(e.target.value);
      }}
			InputProps={{
				endAdornment: <div/>
			}}
		/>
	)
}