import React from 'react';
import { Grid } from '@mui/material';
import { SkeletonLoader } from './SkeletonLoader';

export const SkeletonGrid:React.FC = () => {
	return (
		<Grid container alignItems='stretch' spacing={2} padding={4}>
			{Array.from(Array(12).keys()).map((i) => 
				<Grid item xs={12} sm={6} md={4} lg={3} marginBottom={2}>
					<SkeletonLoader key={i} />
				</Grid>
			)}
		</Grid>
	)
}