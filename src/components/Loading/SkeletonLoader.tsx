import React from 'react';
import styles from '../../styles/Skeleton.module.scss'

export const SkeletonLoader:React.FC = () => (
  <span className={styles.skeletonBox} />
);