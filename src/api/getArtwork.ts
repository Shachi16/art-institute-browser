import { Artwork } from "../types";
import noImage from '../resources/no-image.png';

interface GetArtworkPayload {
    id: string
}

interface GetArtworkSuccessResponse {
    data: Artwork;
}

interface GetArtworkErrorResponse {
    data: null;
    error: string;
}

type GetArtworkResponse = GetArtworkSuccessResponse | GetArtworkErrorResponse;

export const getArtwork = async (payload: GetArtworkPayload): Promise<GetArtworkResponse> => {
	const result = await fetch(`https://api.artic.edu/api/v1/artworks/${payload.id}`).then((response) => {
		return response.json().then((response) => {
			const output: GetArtworkResponse = response.status >= 400 ? 
			{ data: null, error: response.error } : 
			{
				data: { 
					id: response.data.id, 
					title: response.data.title, 
					imageURL: response.data.image_id ? `${response.config.iiif_url}/${response.data.image_id}/full/843,/0/default.jpg` : noImage,
					altTitles: response.data.alt_titles,
					artistDetails: response.data.artist_display,
					placeOfOrigin: response.data.place_of_origin,
					artworkType: response.data.artwork_type_title
				}
			}
			return Promise.resolve(output)
		});
	}).catch(() => {
		return Promise.resolve({ error: 'Sorry, something went wrong', data: null })
	});

  return result
}