interface SearchArtworkPayload {
	searchQuery?: string;
	page: number;
}

interface SearchArtworkSuccessResponse {
	data: {
		ids: []
	};
	totalPages: number;
}

interface SearchArtworkErrorResponse {
	data: null;
	error: string;
}

type SearchArtworkResponse = SearchArtworkSuccessResponse | SearchArtworkErrorResponse;

export const searchArtwork = async (payload: SearchArtworkPayload): Promise<SearchArtworkResponse> => {
	const result = await fetch(`https://api.artic.edu/api/v1/artworks/search?size=12&from=${(payload.page - 1) * 12}&q=${payload.searchQuery}`).then((response) => {
		return response.json().then((response) => {
			const output: SearchArtworkResponse = response.status >= 400 ? 
				{ data: null, error: response.error } : 
				{
					data: { ids: response.data.map((artPiece: any) => artPiece.id)},
					totalPages: response.pagination.total_pages
				}
			return Promise.resolve(output)
		});
	}).catch(() => {
		return Promise.resolve({ error: 'Sorry, something went wrong', data: null })
	});

  return result
}